# c_rusty

A pretty much useless subset C to x86 compiler project written in Rust. Absolutely garbage performance and
atrocious assembly code will abound, but it's all fine.

## Badges are Dope

[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/uses-badges.svg)](https://forthebadge.com)
[![shields.io](https://img.shields.io/badge/Built%20with-Rust-orange.svg?style=for-the-badge&logo=Rust&logoColor=white)](https://shields.io)

## Current Status

[![shields.io](https://img.shields.io/badge/What's%20working%3F-Absolutely%20Nothing!-orange.svg?style=for-the-badge)](https://shields.io) :poop:

[![shields.io](https://img.shields.io/badge/In%20Progress%3F-Absolutely%20Everything!-blue.svg?style=for-the-badge)](https://shields.io) :ok_hand: