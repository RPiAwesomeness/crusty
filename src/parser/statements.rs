use super::expressions;

/// A statement is a self-contained statement within the C program
pub enum Statement {
    // Assignment(statements::Assignment),
    Return(Return),
}

// /// An assignment statement (eg. int foo = 3)
// pub struct Assignment {

// }

// impl Assignment {
//     pub fn new() -> Assignment {

//     }
// }

/// A function statement (eg. int foo() {...}) - ARGUMENTS NOT IMPLEMENTED YET, SINGLE STATEMENT ALLOWED
pub struct Function {
    pub identifier: String,
    // statements: Vec<Statement>,
    pub statement: Option<Statement>,
}

impl Function {
    pub fn new(ident: String, stmt: Option<Statement>) -> Function {
        Function {
            identifier: ident,
            statement: stmt,
        }
    }
}

/// A return statement (eg. return 3) - CURRENTLY ONLY RETURNS INTEGERS
pub struct Return {
    pub value: expressions::Integer,
}

impl Return {
    pub fn new(return_val: expressions::Integer) -> Return {
        Return { value: return_val }
    }
}
