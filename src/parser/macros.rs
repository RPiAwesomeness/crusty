pub struct ExpectedMessage {
    message: String,
}

impl ExpectedMessage {
    pub fn new(msg: &'static str) -> ExpectedMessage {
        ExpectedMessage {
            message: msg.to_string(),
        }
    }

    pub fn raw(&self) -> String {
        self.message.clone()
    }
}

macro_rules! expected {
    ($expected:expr, $got:expr) => {
        format!(
            "Expected {}, got {:?} at line {}, char {}",
            $expected.raw(),
            $got,
            $got.line,
            $got.char_pos
        )
    };
}

macro_rules! get_next {
    ($iter: ident) => {
        match $iter.next() {
            Some(token) => token,
            None => return Err("No further tokens available".to_string()),
        }
    };
}

macro_rules! expect_int {
    ($iter:ident) => {{
        use crate::parser::macros::ExpectedMessage;

        let next_token = get_next!($iter);

        match next_token.token {
            TokenType::Literal(Literal::Integer(val)) => Ok(val),
            _ => {
                return Err(expected!(
                    ExpectedMessage::new("integer literal"),
                    next_token
                ))
            }
        }
    } as Result<i32, String>}
}

macro_rules! expect_ident {
    ($iter:ident) => {{
        use crate::parser::macros::ExpectedMessage;

        let next_token = get_next!($iter).clone();

        match next_token.token {
            TokenType::Literal(Literal::Ident(val)) => Ok(val),
            _ => {
                return Err(expected!(
                    ExpectedMessage::new("identifier literal"),
                    next_token
                ))
            }
        }
    } as Result<String, String>}
}

/// Checks that the current token is the expected type before advancing the iterator
macro_rules! expect {
    ($iter:ident: $match_type:expr) => {
        let next_token = match $iter.next() {
            Some(token) => token,
            None => return Err("No further tokens available".to_string()),
        };

        if next_token.token != $match_type {
            return Err(expected!($match_type, next_token));
        }
    };
}

macro_rules! terminate {
    ($token:ident) => {
        expect!($token: TokenType::Delimeter(Delimeter::Semicolon));
    };
}
