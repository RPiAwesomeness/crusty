pub mod expressions;
pub mod generator;
pub mod statements;
pub mod types;

#[macro_use]
mod macros;

use super::lexer::tokens::{Delimeter, Keyword, Literal, Token, TokenType};

/// Stores the data about each program
pub struct Program {
    Statement: statements::Function,
}

impl Program {
    pub fn new(statement: statements::Function) -> Program {
        Program {
            Statement: statement,
        }
    }
}

/// An expression is anything in a program
enum Expression {
    Constant(expressions::Integer),
}

/// Converts vector of tokens to Program
pub fn parse(source: Vec<Token>) -> Result<Program, String> {
    let mut iter = source.iter().peekable();

    expect!(iter: TokenType::Keyword(Keyword::Integer));
    let main_ident = match expect_ident!(iter)?.as_str() {
        "main" => "main".to_string(),
        _ => return Err("First function identifier is not main".to_string()),
    };
    expect!(iter: TokenType::Delimeter(Delimeter::OpenParen));
    expect!(iter: TokenType::Delimeter(Delimeter::CloseParen));
    expect!(iter: TokenType::Delimeter(Delimeter::OpenBrace));
    expect!(iter: TokenType::Keyword(Keyword::Return));
    let int_literal = expect_int!(iter)?;
    terminate!(iter);
    expect!(iter: TokenType::Delimeter(Delimeter::CloseBrace));

    let ret_stmt = statements::Return::new(expressions::Integer::new(int_literal));
    let stmt_opt = Some(statements::Statement::Return(ret_stmt));
    let main_func = statements::Function::new(main_ident, stmt_opt);

    Ok(Program::new(main_func))
}
