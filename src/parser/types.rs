use std::fmt;

/// All basic types must implement Type trait
pub trait Type: fmt::Debug {
    type ItemType;

    fn get_value(&self) -> Self::ItemType;
    fn as_string(&self) -> String;
}
