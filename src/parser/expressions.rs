use super::types;

/// Stores information related to signed integers in C
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Integer {
    pub value: i32,
}

impl types::Type for Integer {
    type ItemType = i32;

    fn get_value(&self) -> Self::ItemType {
        self.value
    }

    fn as_string(&self) -> String {
        self.value.clone().to_string()
    }
}

impl Integer {
    pub fn new(val: i32) -> Self {
        Integer { value: val }
    }
}

/// Stores identifier string and a Type object as a value
pub struct Identifier<T: types::Type> {
    pub identifier: String,
    pub value: Box<T>,
}
