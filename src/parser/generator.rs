//! Code related to generation of x86 ASM from C

use super::statements::Statement;
use super::types::Type;
use super::Program;

/// Value to be  filled is constant to be returned
const RETURN_CONST_TEMPLATE: &'static str = "movl   ${RET_VAL}, %eax\nret";

/// Generates the appropriate x86 ASM code in string form from a Program
///
/// # Arguments
///
/// - `program` - Program object to generate the assembly from
///
/// # Returns
///
/// A string of x86 ASM source
pub fn generate(program: Program) -> String {
    // Currently only a single statement, load the return template with the value
    let template = include_str!("../templates/main.s");
    let ret_statement: Statement = program.Statement.statement.expect("Expected a statement");
    match ret_statement {
        Statement::Return(ret) => {
            RETURN_CONST_TEMPLATE.replace("{RET_VAL}", ret.value.as_string().as_str())
        }
    }
}
