use std::io::{self, BufRead, Write};

use clap::clap_app;

mod lexer;
mod parser;

const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const AUTHOR: &'static str = "RPiAwesomeness";
const ABOUT: &'static str = "Pretty much useless C subset compiler written in Rust";

fn main() {
    let stdin = io::stdin();

    // Read command line arguments
    let matches = clap_app!(c_rusty =>
        (version: VERSION.unwrap_or("unknown"))
        (author: AUTHOR)
        (about: ABOUT)
        (@arg source: "Path to C source file to compile")
        (@arg verbose: -v --verbose "Verbosity of output during compilation")
        (@arg version: --version "Returns version of c_rusty in use")
    )
    .get_matches();

    if matches.occurrences_of("version") == 1 {
        // Display version
        println!("c_rusty version {}", VERSION.unwrap_or("unknown"));
        println!("{}", AUTHOR);
        println!("{}", ABOUT);
        return;
    }

    if matches.occurrences_of("v") == 0 {
        // Set verbose mode
    }

    println!("Welcome to c_rusty!");

    loop {
        // Prompt for getting source to tokenize/parse
        print!(">> ");
        io::stdout().flush().expect("Error flushing stdout");

        // Get source line from user
        let mut line = String::new();
        stdin
            .lock()
            .read_line(&mut line)
            .expect("Error reading from stdin");

        // Tokenize source and then print out tokens
        let tokens = lexer::tokenize_source(&line);
        for tok in tokens.iter() {
            println!("{:?}", tok);
        }

        // Parse tokenized values
        let result = parser::parse(tokens);
        if let Ok(parsed) = result {
            println!("Parsed successfully!");
        } else if let Err(err) = result {
            // Error occurred. Display it and then nope the nope outta there
            eprintln!("Error occured during parsing:");
            eprintln!("{}", err);
            continue;
        }
    }
}
