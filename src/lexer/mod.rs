pub mod tokens;

use self::tokens::{Control, Delimeter, Literal, Operator, Token, TokenType};

use std::iter::Peekable;
use std::str::Chars;

/// Handles the tokenization of the source file
pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>,
    absolute: u32,
    char_pos: u32,
    line: u32,
}

impl<'a> Lexer<'a> {
    /// Creates a new Lexer with the input string converted into a Peekable of chars
    ///
    /// # Arguments
    ///
    /// - `source` - Raw text source of program to lex
    ///
    /// # Returns
    ///
    /// Lexer with untouched source iterator and initial positions set
    pub fn new(source: &str) -> Lexer {
        Lexer {
            input: source.chars().peekable(),
            absolute: 0,
            char_pos: 0,
            line: 1,
        }
    }

    /// Attempts to read the next character in the input
    ///
    /// # Returns
    ///
    /// Some(char) if there is another character available
    fn read_char(&mut self) -> Option<char> {
        // Incremement the character and absolute position
        self.char_pos += 1;
        self.absolute += 1;

        // Read and return the next character
        self.input.next()
    }

    /// Peeks at the next character in the input and returns a reference to that char without consuming it
    fn peek_char(&mut self) -> Option<&char> {
        self.input.peek()
    }

    /// Peeks at the next character in the input and compares it against the character provided
    fn peek_char_is(&mut self, ch: char) -> bool {
        match self.peek_char() {
            Some(&c) => c == ch,
            None => false,
        }
    }

    /// Consumes characters until a non-whitespace character is detected
    fn skip_whitespace(&mut self) {
        while let Some(&c) = self.peek_char() {
            // If the peeked character is non-whitespace...
            if !c.is_whitespace() {
                // ...stop skipping
                break;
            }

            self.read_char();

            // If a new line is hit then increment the line count and reset the character position
            if c == '\n' {
                self.line += 1;
                self.char_pos = 0;
            }
        }
    }

    /// Non-consumingly checks if the next character is a letter
    fn peek_is_letter(&mut self) -> bool {
        match self.peek_char() {
            Some(&ch) => ch.is_alphabetic(),
            None => false,
        }
    }

    /// Non-consumingly checks if the next character is a valid next character in an identifier
    fn peek_is_valid_identifier(&mut self) -> bool {
        self.peek_is_letter() || self.peek_char_is('_')
    }

    /// Reads an identifier into a string, consuming characters
    fn read_identifier(&mut self, first: char) -> String {
        // Create the String we'll be returning
        let mut ident = String::new();
        ident.push(first);

        // Keep going while the characters are alphanumeric
        while self.peek_is_valid_identifier() {
            ident.push(self.read_char().unwrap());
        }

        // Return the identifier
        ident
    }

    /// Reads characters until non-numeric characters are reached
    fn read_number(&mut self, first: char) -> i32 {
        let mut number = String::new();
        number.push(first);

        while let Some(&c) = self.peek_char() {
            if !c.is_numeric() {
                break;
            }

            number.push(self.read_char().unwrap());
        }

        match number.parse::<i32>() {
            Ok(num) => num,
            Err(err) => panic!("Invalid number somehow...\n{}", err),
        }
    }

    fn next_token(&mut self) -> Token {
        // Skip leading whitespace
        self.skip_whitespace();

        let mut token = Token {
            token: TokenType::Control(Control::Illegal),
            absolute: self.absolute,
            char_pos: self.char_pos,
            line: self.line,
        };

        match self.read_char() {
            // Delimeters
            Some('(') => {
                token.token = TokenType::Delimeter(Delimeter::OpenParen);
                token
            }
            Some(')') => {
                token.token = TokenType::Delimeter(Delimeter::CloseParen);
                token
            }
            Some('{') => {
                token.token = TokenType::Delimeter(Delimeter::OpenBrace);
                token
            }
            Some('}') => {
                token.token = TokenType::Delimeter(Delimeter::CloseBrace);
                token
            }
            Some(';') => {
                token.token = TokenType::Delimeter(Delimeter::Semicolon);
                token
            }
            Some('=') => {
                token.token = TokenType::Operator(Operator::Assign);
                token
            }
            // Other
            Some(ch @ _) => {
                if is_valid_identifier_char(ch) {
                    let literal = self.read_identifier(ch);
                    token.token = tokens::lookup_ident(literal);
                    token
                } else if ch.is_numeric() {
                    let num = self.read_number(ch);
                    token.token = TokenType::Literal(Literal::Integer(num));
                    token
                } else {
                    let mut foo = String::new();
                    foo.push(ch);
                    println!("Unexpected (illegal) character: {}", foo);
                    token.token = TokenType::Control(Control::Illegal);
                    token
                }
            }

            // Handle EOF
            None => {
                token.token = TokenType::Control(Control::EndOfFile);
                token
            }
        }
    }
}

/// Tokenizes the source string passed to it into a vector of type Token
///
/// # Arguments
///
/// - `source` - String of source to tokenize
///
/// # Returns
///
/// Vector of Token objects
pub fn tokenize_source(source: &String) -> Vec<Token> {
    let mut tokens: Vec<Token> = Vec::new();
    let mut lexer = Lexer::new(source);

    loop {
        let tok = lexer.next_token();
        if tok.token == TokenType::Control(Control::EndOfFile) {
            break;
        }
        tokens.push(tok);
    }

    tokens
}

/// Validates whether the passed character is a valid identifier
fn is_valid_identifier_char(ch: char) -> bool {
    ch.is_alphabetic() || ch == '_'
}
