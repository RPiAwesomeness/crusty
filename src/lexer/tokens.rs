#[derive(Clone, Debug, PartialEq)]
pub enum TokenType {
    Keyword(Keyword),
    Control(Control),
    Delimeter(Delimeter),
    Literal(Literal),
    Operator(Operator),
}

impl TokenType {
    pub fn raw(&self) -> String {
        match self {
            TokenType::Keyword(keyword) => keyword.raw(),
            TokenType::Control(ctrl) => ctrl.raw(),
            TokenType::Delimeter(delim) => delim.raw(),
            TokenType::Literal(literal) => literal.raw(),
            TokenType::Operator(op) => op.raw(),
        }
    }

    pub fn name(&self) -> String {
        match self {
            TokenType::Keyword(keyword) => keyword.name(),
            TokenType::Control(ctrl) => ctrl.name(),
            TokenType::Delimeter(delim) => delim.name(),
            TokenType::Literal(literal) => literal.name(),
            TokenType::Operator(op) => op.name(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Keyword {
    Integer,
    Return,
}

impl Keyword {
    fn raw(&self) -> String {
        match self {
            Keyword::Integer => "int",
            Keyword::Return => "return",
        }
        .to_string()
    }

    fn name(&self) -> String {
        match self {
            Keyword::Integer => "integer keyword",
            Keyword::Return => "return",
        }
        .to_string()
    }

    fn token_type(&self) -> String {
        "keyword".to_string()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Control {
    EndOfFile,
    Illegal,
}

impl Control {
    fn raw(&self) -> String {
        match self {
            Control::EndOfFile => "EOF",
            Control::Illegal => "ILLEGAL",
        }
        .to_string()
    }

    fn name(&self) -> String {
        match self {
            Control::EndOfFile => "EOF",
            Control::Illegal => "ILLEGAL",
        }
        .to_string()
    }

    fn token_type(&self) -> String {
        "control".to_string()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Delimeter {
    OpenBrace,  // {
    CloseBrace, // }
    OpenParen,  // (
    CloseParen, // )
    Semicolon,  //;
}

impl Delimeter {
    fn raw(&self) -> String {
        match self {
            Delimeter::OpenBrace => "{",
            Delimeter::CloseBrace => "}",
            Delimeter::OpenParen => "(",
            Delimeter::CloseParen => ")",
            Delimeter::Semicolon => ";",
        }
        .to_string()
    }

    fn name(&self) -> String {
        match self {
            Delimeter::OpenBrace => "open brace",
            Delimeter::CloseBrace => "close brace",
            Delimeter::OpenParen => "open parenthesis",
            Delimeter::CloseParen => "close parenthesis",
            Delimeter::Semicolon => "semicolon",
        }
        .to_string()
    }

    fn token_type(&self) -> String {
        "delimeter".to_string()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Operator {
    Assign, // =
}

impl Operator {
    fn raw(&self) -> String {
        match self {
            Operator::Assign => "=",
        }
        .to_string()
    }

    fn name(&self) -> String {
        match self {
            Operator::Assign => "assignment operator",
        }
        .to_string()
    }

    fn token_type(&self) -> String {
        "operator".to_string()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Literal {
    Integer(i32),
    Ident(String),
}

impl Literal {
    fn raw(&self) -> String {
        match self {
            Literal::Ident(ident) => ident.clone(),
            Literal::Integer(int) => int.to_string(),
        }
    }

    fn name(&self) -> String {
        match self {
            Literal::Ident(_) => "identifier",
            Literal::Integer(_) => "integer",
        }
        .to_string()
    }

    fn token_type(&self) -> String {
        "literal".to_string()
    }
}

/// Keeps track of a token with the character position and line it exists it
#[derive(Clone, Debug, PartialEq)]
pub struct Token {
    pub token: TokenType,
    pub absolute: u32,
    pub char_pos: u32,
    pub line: u32,
}

/// Takes an identifier string and attempts to match it against defined keywords.
///
/// # Arguments
///
/// - `identifier` - String of identifier to match
///
/// # Returns
///
/// Keyword TokenType that matches the passed the identifier, otherwise if it matches no keywords then an
/// Ident TokenType storing the value that was passed in
pub fn lookup_ident(identifier: String) -> TokenType {
    match identifier.as_str() {
        "int" => TokenType::Keyword(Keyword::Integer),
        "return" => TokenType::Keyword(Keyword::Return),
        _ => TokenType::Literal(Literal::Ident(identifier)),
    }
}
